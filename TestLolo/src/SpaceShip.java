
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class SpaceShip extends Sprite {
public int mdetector = 0;
    private int dx;
    private int dy;
    private List<Missile> missiles;

    public SpaceShip(int x, int y) {
        super(x, y);

        initCraft();
    }
    public void setmdetector(int mdetector) {
    	this.mdetector = mdetector;
    }

    private void initCraft() {
        
        missiles = new ArrayList<>();
        loadImage("sprite/sprite/lorann_b.png");
        getImageDimensions();
    }

    public void move() {

        x += dx;
        y += dy;

        if (x < 0) {
            x = 0;
        }

        if (y < 0) {
            y = 0;
           
        }
        if (x > 955)
        	x = 955;
        if(y > 955)
        	y= 955;
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_SPACE) {
        	if(mdetector==0) {
            fire();
            mdetector = 1;
        	}
        }

        if (key == KeyEvent.VK_LEFT) {
            dx = -32;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 32;
        }

        if (key == KeyEvent.VK_UP) {
            dy = -32;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 32;
        }
    
    }

    public void fire() {
        missiles.add(new Missile(x + width, y + height / 2));
    }
;
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }
    
}