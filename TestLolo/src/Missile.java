
public class Missile extends Sprite {

    private final int BOARD_WIDTH = 1000;
    public int com = 1;
    public Missile(int x, int y) {
        super(x, y);

        initMissile();
    }
    
    private void initMissile() {
        
        loadImage("sprite/sprite/fireball_1.png");
        getImageDimensions();        
    }

    public void move() {
    	
        
        if (x > BOARD_WIDTH)
            visible = false;

            if (x < 32) {
              com = 1;
            }
            if (x > 945) {
                com = 2;
              }
            
            if (com == 1) {
                x += 32;
              }
            if (com == 2) {
                x -= 32;
              }
    
    
    }
}
