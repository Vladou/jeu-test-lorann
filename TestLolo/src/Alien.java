
public class Alien extends Sprite {

    private final int INITIAL_X = 400;
    public int com = 1;
    public Alien(int x, int y) {
        super(x, y);

        initAlien();
    }

    private void initAlien() {

        loadImage("sprite/sprite/monster_1.png");
        getImageDimensions();
    }

    public void move() {

        if (x < 32) {
          com = 2;
        }
       
        if (x > 930 ) {
            com = 1;
        }
        
        if (com == 2) {
            x += 32;
          }
        if (com == 1) {
            x -= 32;
          }
        
    }
}

